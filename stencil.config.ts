import { Config } from '@stencil/core';

// https://stenciljs.com/docs/config

export const config: Config = {
  globalScript: 'src/global/app.ts',
  globalStyle: 'src/global/app.css',
  taskQueue: 'async',
  namespace: 'bar-menu',
  outputTargets: [{
    type: 'www',
    serviceWorker: null
  },
    { type: 'dist' }
  ],
};
