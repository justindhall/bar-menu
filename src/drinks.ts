export const COCKTAILS: any[] = [
  {
    name: 'Sazerac',
    description: 'A powerful, brawny cocktail of rye, absinthe, and Peychaud\'s bitters, the Sazerac is timeless for a reason.',
    url: 'https://www.seriouseats.com/recipes/2007/07/cocktails-recipes-the-sazerac.html',
    image: 'assets/images/sazerac.jpg'
  },
  {
    name: 'Boulevardier',
    description: 'Essentially a Negroni with whiskey swapped in for gin, this classic is a beautiful cocktail in its own right.',
    url: 'https://www.seriouseats.com/recipes/2008/09/boulevardier-recipe.html',
    image: 'assets/images/boulevardier.jpg'
  },
  {
    name: 'Negroni',
    description: 'Crisp and bitter, the classic cocktail of gin, Campari, and sweet vermouth is dependably delicious, and remarkably difficult to foul up.',
    url: 'https://www.seriouseats.com/recipes/2010/04/negroni-cocktail-recipe-gin-campari-vermouth.html',
    image: 'assets/images/negroni.jpg'
  },
  {
    name: 'Paper Plane',
    description: 'A little sweet, a little bitter, a little fresh, and a little too easy drinking.',
    url: 'https://www.seriouseats.com/recipes/2012/02/the-paper-plane-from-the-hawthorne-bourbon-aperol-cocktail-recipe.html',
    image: 'assets/images/paper-plane.jpeg'
  },
  {
    name: 'French 75',
    description: 'The combination of gin, lemon juice, and Champagne brings out the best in each.',
    url: 'https://www.seriouseats.com/recipes/2011/03/french-75-cocktial-gin-champagne-lemon.html',
    image: 'assets/images/french-75.jpg'
  },
  {
    name: 'Aperol Spritz',
    description: 'Just a little bitter, just a little sweet, and irresistible any time of day.',
    url: 'https://www.seriouseats.com/recipes/2011/06/aperol-spritz-recipe.html',
    image: 'assets/images/aperol-spritz.jpg'
  },
  {
    name: 'Filibuster',
    description: 'Erik Adkins uses the basic Whiskey Sour blueprint for this autumn-inspired drink.',
    url: 'https://www.seriouseats.com/recipes/2012/12/filibuster-cocktail-erik-adkins-whiskey-sour-rye-maple-recipe.html',
    image: 'assets/images/filibuster.jpg'
  },
  {
    name: "Bee's Knees",
    description: 'A Prohibition-era cocktail designed to mask a bad gin, but delicious with a great one.',
    url: 'https://www.seriouseats.com/recipes/2008/12/bees-knees-recipe-drinks-cocktails-gin.html',
    image: 'assets/images/bees-knees.jpg'
  }
]

