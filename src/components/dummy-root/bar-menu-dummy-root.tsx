import { Component, h } from '@stencil/core';

@Component({
  tag: 'bar-menu-dummy-root',
  styleUrl: 'bar-menu-dummy-root.css',
})
export class BarMenuDummyRoot {
  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="bar-menu-fake-profile" />
          <ion-route url="/drink" component="menu-item" />
          <ion-route url="/menu" component="menu-list" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
