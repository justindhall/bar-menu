import {Component, h} from '@stencil/core';

@Component({
  tag: 'bar-menu-fake-profile',
  styleUrl: 'list.css'
})
export class FakeProfile {

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
          </ion-buttons>
          <ion-title>Fake Profile</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <ion-list >
          <ion-item-divider>
            <ion-label>
              Development
            </ion-label>
          </ion-item-divider>
          <ion-item button={true} href={`/menu`}>
            <ion-label>
              Bar Menu
            </ion-label>
          </ion-item>
        </ion-list>
      </ion-content>
    ];
  }
}
