import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'menu-item',
})
export class MenuItem {
  @Prop() cocktail: any;
  params: any;

  componentWillRender() {
    this.params = this.urlParams()
  }

  urlParams() {
    const search = location.search.split('?')[1];
    if (search === undefined) { return {}; }
    return JSON.parse(
      '{"' + decodeURI(search).replace(/"/g, '\\"'
      ).replace(/&/g, '","')
        .replace(/=/g, '":"') + '"}');
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button href='/menu'>
              <ion-icon name="arrow-back-outline" />
            </ion-button>
          </ion-buttons>
          <ion-title>{this.params.name}</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
          <iframe src={this.params.url} style={{position: 'absolute', top: '0', left: '0', width: '100%', height: '100%'}}/>
      </ion-content>
    ];
  }
}
