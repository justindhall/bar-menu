import { Component, h } from '@stencil/core';
import { COCKTAILS} from "../../../drinks";

@Component({
  tag: 'menu-list',
})
export class MenuList {

  renderDrinks() {
    return COCKTAILS.map((cocktail) => {
      return [
        <menu-list-item cocktail={cocktail} />
      ]
    });
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar >
          <ion-buttons slot='start'>
            <ion-button href='/'>
              <ion-icon name="arrow-back-outline" />
            </ion-button>
          </ion-buttons>
          <ion-title>Menu</ion-title>
        </ion-toolbar>
      </ion-header>,

      <ion-content class="ion-padding">
        {this.renderDrinks()}
      </ion-content>,
    ];
  }
}
