import {Component, h, Prop} from '@stencil/core';

@Component({
  tag: 'menu-list-item',
})
export class MenuListItem {
  @Prop() cocktail: any;

  urlParameters() {
    return Object.entries(this.cocktail).map(e => e.join('=')).join('&')
  }

render() {
    return [
      <ion-card button={true} href={`/drink/?${this.urlParameters()}`}>
        <ion-img src={this.cocktail.image} />
        <ion-card-header>
          <ion-card-title>
            {this.cocktail.name}
          </ion-card-title>
        </ion-card-header>
        <ion-card-content>
          {this.cocktail.description}
        </ion-card-content>
      </ion-card>
    ];
  }
}
